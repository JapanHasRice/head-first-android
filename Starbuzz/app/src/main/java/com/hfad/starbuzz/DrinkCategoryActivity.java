package com.hfad.starbuzz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class DrinkCategoryActivity extends AppCompatActivity {
    private SQLiteDatabase db;
    private Cursor allDrinksCursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_category);
        new GetAllDrinksTask().execute();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        allDrinksCursor.close();
        db.close();
    }

    private class GetAllDrinksTask extends AsyncTask<Void, Void, Boolean> {

        protected void onPreExecute() {
            ListView listDrinks = findViewById(R.id.list_drinks);

            AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(DrinkCategoryActivity.this,
                            DrinkActivity.class);
                    intent.putExtra(DrinkActivity.EXTRA_DRINKID, (int) id);
                    startActivity(intent);
                }
            };

            listDrinks.setOnItemClickListener(listener);
        }

        protected Boolean doInBackground(Void... voids) {
            try {
                SQLiteOpenHelper starbuzzDatabaseHelper = new StarbuzzDatabaseHelper(DrinkCategoryActivity.this);
                db = starbuzzDatabaseHelper.getReadableDatabase();
                allDrinksCursor = db.query("DRINK",
                        new String[] {"_id", "NAME"},
                        null, null, null, null, null);
                return true;
            } catch (SQLiteException e) {
                return false;
            }
        }

        protected void onPostExecute(Boolean success) {
            if (success) {
                ListView listDrinks = findViewById(R.id.list_drinks);
                SimpleCursorAdapter listAdapter = new SimpleCursorAdapter(DrinkCategoryActivity.this,
                        android.R.layout.simple_list_item_1,
                        allDrinksCursor,
                        new String[] {"NAME"},
                        new int[] {android.R.id.text1},
                        0);

                listDrinks.setAdapter(listAdapter);
            } else {
                Toast toast = Toast.makeText(DrinkCategoryActivity.this, "Could not retrieve Drinks", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }
}
