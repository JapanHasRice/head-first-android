package com.hfad.starbuzz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class TopLevelActivity extends AppCompatActivity {
    private SQLiteDatabase db;
    private Cursor favoritesCursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_level);
        setupCategoryListView();
        setupFavoritesListView();
    }

    private void setupCategoryListView() {
        AdapterView.OnItemClickListener categoryClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View itemView, int position, long id) {
                if (position == 0) {
                    Intent intent = new Intent(TopLevelActivity.this, DrinkCategoryActivity.class);
                    startActivity(intent);
                }
            }
        };

        ListView categoryListView = findViewById(R.id.list_options);
        categoryListView.setOnItemClickListener(categoryClickListener);
    }

    private void setupFavoritesListView() {
        ListView favoriteListView = findViewById(R.id.list_favorites);
        AdapterView.OnItemClickListener favoritesClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(TopLevelActivity.this, DrinkActivity.class);
                intent.putExtra(DrinkActivity.EXTRA_DRINKID, (int) id);
                startActivity(intent);
            }
        };
        favoriteListView.setOnItemClickListener(favoritesClickListener);

        new PopulateFavoritesListTask().execute();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        favoritesCursor.close();
        db.close();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        new PopulateFavoritesListTask().execute();
    }

    private class PopulateFavoritesListTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                if (db == null) {
                    db = new StarbuzzDatabaseHelper(TopLevelActivity.this).getReadableDatabase();
                }
                favoritesCursor = db.query("DRINK",
                        new String[] {"_id", "NAME"},
                        "FAVORITE = ?",
                        new String[] {"1"},
                        null, null, null);
                return true;
            } catch (SQLiteException e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success) {
                ListView favoriteListView = findViewById(R.id.list_favorites);

                SimpleCursorAdapter cursorAdapter = (SimpleCursorAdapter) favoriteListView.getAdapter();
                if (cursorAdapter == null) {
                    cursorAdapter = new SimpleCursorAdapter(TopLevelActivity.this,
                            android.R.layout.simple_list_item_1,
                            favoritesCursor,
                            new String[] {"NAME"},
                            new int[] {android.R.id.text1});
                    favoriteListView.setAdapter(cursorAdapter);
                } else {
                    cursorAdapter.changeCursor(favoritesCursor);
                }
            } else {
                Toast toast = Toast.makeText(TopLevelActivity.this, "Could not retrieve favorites", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }
}
